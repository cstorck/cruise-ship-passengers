<?php
class Passengers extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('Passengers_model');
                $this->load->helper('url_helper');
        }


        public function view($LastName = NULL)
        {
        

	        $data['passenger_item'] = $this->Passengers_model->get_passengers($LastName); 
	
		
		if(empty($data['passenger_item'])){
			show_404();
		}	
	
		$data['title'] = $data['passenger_item']["LastName"]; 

		$this->load->view('templates/header', $data);
        	$this->load->view('passengers/view', $data);
	        $this->load->view('templates/footer'); 

	}


	public function index()
	{
	$data['passengers'] = $this->Passengers_model->get_passengers();
	$data['title'] = "Passenger Info"; 
	
	$this->load->view('templates/header', $data);
        $this->load->view('passengers/index', $data);
        $this->load->view('templates/footer');
}



public function create()
{
    $this->load->helper('form');
    $this->load->library('form_validation');

    $data['title'] = 'Create a new passenger';

    $this->form_validation->set_rules('LastName', 'Last Name', 'required');
    $this->form_validation->set_rules('FirstName', 'First Name', 'required');

    if ($this->form_validation->run() === FALSE)
    {
        $this->load->view('templates/header', $data);
        $this->load->view('passengers/create');
        $this->load->view('templates/footer');

    }
    else
    {
        $this->Passengers_model->set_passenger();
    	
        $this->load->view("passengers/success"); 

	}
}


}
