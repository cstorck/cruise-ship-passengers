<?php
class Passengers_model extends CI_Model{ 
        public function __construct()
        {
                $this->load->database();
        }

	public function get_passengers($LastName = FALSE)
	{
        	error_log("In get.... ($LastName) \n"); 
		if ($LastName === FALSE)
        	{
                	$query = $this->db->get('passengers');
                	return $query->result_array();
        	}

		error_log("We have a last name, $LastName , \n"); 
        	$query = $this->db->get_where('passengers', array('LastName' => $LastName));
        	return $query->row_array();
	}


public function set_passenger()
{
    $this->load->helper('url');

    $data = array(
        'LastName' => $this->input->post('LastName'),
		'FirstName' => $this->input->post('FirstName'),
		'PhoneNumber' => $this->input->post('PhoneNumber'),
		'RoomNumber' => $this->input->post('RoomNumber'),
		'CruiseID' => $this->input->post('CruiseID'),
		'hasDrinks' => $this->input->post('hasDrinks'),
		'hasInternet' => $this->input->post('hasInternet') 
    );

    return $this->db->insert('passengers', $data);
}

}
